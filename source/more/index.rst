.. _doc-android-more:

****
More
****

:ref:`doc-android-settings` 
:ref:`doc-android-network` 

.. toctree::
    :name: toc-android-more

    settings/index.rst
    network/index.rst